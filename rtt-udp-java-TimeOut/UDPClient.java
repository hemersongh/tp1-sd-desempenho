import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

class UDPClient {
	public static double getMax(ArrayList<Long> listTimeSends) {
		double greatest = listTimeSends.get(0);

		for (long times : listTimeSends) {
			if (times > greatest) {
				greatest = times;
			}
		}

		return greatest / 1000000;
	}

	public static double getMin(ArrayList<Long> listTimeSends) {
		double smaller = listTimeSends.get(0);

		for (long times : listTimeSends) {
			if (times < smaller) {
				smaller = times;
			}
		}

		return smaller / 1000000;
	}

	public static double getStandardDeviation(ArrayList<Long> listTimeSends) {
		double sandardDeviation = 0, sum = 0;

		for (long times : listTimeSends) {
			sum += times;
		}

		double mean = sum / (listTimeSends.size());

		for (long times : listTimeSends) {
			sandardDeviation += Math.pow((times - mean), 2);
		}

		return ((double) Math.sqrt(sandardDeviation / (listTimeSends.size()))) / 1000000;
	}

	public static double getAvarage(ArrayList<Long> listTimeSends) {
		double sum = 0;

		for (long times : listTimeSends) {
			sum += times;
		}

		return (sum / (listTimeSends.size())) / 1000000;
	}

	@SuppressWarnings("resource")
	public static void main(String args[]) throws Exception {
		long startTime, stopTime, elapsedTime;
		String serverHostname = null, sentence = "Test";
		ArrayList<Long> listTimesSend = new ArrayList<Long>();
		int quantityTimeOut = 0, packageReceive = 0, packageSends = 0, quantityTimesRound = 200, port = 5700;

		Scanner in = new Scanner(System.in);

		System.out.print("\n" + "Enter the ip adress: ");
		serverHostname = in.nextLine();

		System.out.print("Enter the port: ");
		port = in.nextInt();

		System.out.print("Type how many times you want to send: ");
		quantityTimesRound = in.nextInt();

		System.out.println();

		try {
			if (args.length > 0) {
				serverHostname = args[0];
			}

			DatagramSocket clientSocket = new DatagramSocket();
			InetAddress IPAddress = InetAddress.getByName(serverHostname);

			System.out.println("------------------------------------------------------------");
			System.out.println("Attemping to connect to " + IPAddress + " via UDP port " + port);

			byte[] sendData = new byte[1024];
			byte[] receiveData = new byte[1024];

			for (int i = 0; i < quantityTimesRound; i++) {
				Thread.sleep(500);
				sendData = sentence.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
				startTime = System.nanoTime();
				clientSocket.send(sendPacket);
				packageSends++;
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				clientSocket.setSoTimeout(100);

				try {
					clientSocket.receive(receivePacket);
					stopTime = System.nanoTime();
					elapsedTime = stopTime - startTime;
					listTimesSend.add(elapsedTime);
					packageReceive++;
				} catch (SocketTimeoutException ste) {
					quantityTimeOut++;
				}
			}

			clientSocket.close();
		} catch (

		UnknownHostException ex) {
			System.err.println(ex);
		} catch (IOException ex) {
			System.err.println(ex);
		}

		System.out.println("\n\n" + "------------------------------------------------------------");
		System.out.println("--------- Ping statistics to " + serverHostname + " ----------------");
		System.out.println("--------- Packages: ");
		System.out.println("------------------ Send = " + packageSends);
		System.out.println("------------------ Receive = " + packageReceive);
		System.out.println("------------------ Lost = " + quantityTimeOut + " ("
				+ ((float) (quantityTimeOut * 100) / quantityTimesRound) + "% lost)");
		System.out.println("------------------ Timeout occurred: " + quantityTimeOut + " times");
		System.out.println("------------------------------------------------------------");

		if (!listTimesSend.isEmpty()) {
			System.out.println("--------- Time To Send Packages: ");
			System.out.printf("------------------ Avarage: %2.6f %s %s", getAvarage(listTimesSend), "milliseconds",
					"\n");
			System.out.printf("------------------ Min: %2.6f %s %s", getMin(listTimesSend), "milliseconds", "\n");
			System.out.printf("------------------ Max: %2.6f %s %s", getMax(listTimesSend), "milliseconds", "\n");
			System.out.printf("------------------ Standard Deviation: %2.6f %s %s", getStandardDeviation(listTimesSend),
					"milliseconds", "\n");
		}

		System.out.println("------------------------------------------------------------");
	}
}
