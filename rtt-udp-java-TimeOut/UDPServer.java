import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

class UDPServer {
	@SuppressWarnings("resource")
	public static void main(String args[]) throws Exception {
		try {
			DatagramSocket serverSocket = new DatagramSocket(7750);

			byte[] receiveData = new byte[1024];
			byte[] sendData = new byte[1024];

			System.out.println("------------------------------------------------------------");
			System.out.println("---------------- Waiting for datagram packet ---------------");

			while (true) {
				receiveData = new byte[1024];
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);
				String sentence = new String(receivePacket.getData());
				InetAddress IPAddress = receivePacket.getAddress();
				int port = receivePacket.getPort();
				sendData = sentence.toUpperCase().getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
				serverSocket.send(sendPacket);
			}

		} catch (SocketException ex) {
			System.out.println(ex);
			System.exit(1);
		}

		System.out.println("------------------------------------------------------------");
	}
}
