import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = null;
		int port = 7775;

		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.err.println("Could not listen on port: " + port);
			System.exit(1);
		}

		Socket clientSocket = null;

		System.out.println("------------------------------------------------------------");
		System.out.println("Waiting for connection.....");

		try {
			clientSocket = serverSocket.accept();
		} catch (IOException e) {
			System.err.println("Accept failed.");
			System.exit(1);
		}

		System.out.println("Connection successful");
		System.out.println("Waiting for input.....");
		System.out.println("------------------------------------------------------------");
		System.out.println("Client is sending packet");
		System.out.print("Receiving...");

		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			out.println("OK");
		}

		System.out.println("\n" + "------------------------------------------------------------");

		out.close();
		in.close();
		clientSocket.close();
		serverSocket.close();
	}
}
