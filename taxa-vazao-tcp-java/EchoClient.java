import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class EchoClient {
	public static void main(String[] args) throws IOException {
		String serverHostname = null;
		int port = 5500, quantityTimesRound = 5120, quantityTimesRounded = quantityTimesRound;
		long startTime;

		char[] sentence = new char[1024];

		for (int i = 0; i < sentence.length - 2; i++) {
			sentence[i] = 'a';
		}

		sentence[1022] = '\\';
		sentence[1023] = 'n';

		@SuppressWarnings("resource")
		Scanner scannerIn = new Scanner(System.in);

		System.out.print("\n" + "Enter the ip adress: ");
		serverHostname = scannerIn.nextLine();

		System.out.print("Enter the port: ");
		port = scannerIn.nextInt();

		System.out.print("Type how many times you want to send: ");
		quantityTimesRound = scannerIn.nextInt();
		quantityTimesRounded = quantityTimesRound;
		System.out.println();

		Socket echoSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		try {
			System.out.println("------------------------------------------------------------");
			System.out.println("Attemping to connect to host " + serverHostname + " via TCP on port " + port);
			System.out.println("------------------------------------------------------------");

			echoSocket = new Socket(serverHostname, port);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: " + serverHostname);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for " + "the connection to: " + serverHostname);
			System.exit(1);
		}

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

		startTime = System.nanoTime();

		System.out.println("Sending");

		while (quantityTimesRound != 0) {
			out.println(sentence);
			in.readLine();
			quantityTimesRound--;
		}

		long stopTime = System.nanoTime();
		long elapsedTime = stopTime - startTime;
		double sizeOfPackage = ((((((sentence.length * 8) * quantityTimesRounded))) / 1024) / 1024);
		double elapsedTimeToSend = sizeOfPackage / (elapsedTime / 1000000000);

		System.out.println("\n" + "------------------------------------------------------------");
		System.out.println("\n" + "------------------------ Flow Rate --------------------------");
		System.out.println("------------------ Size of file: " + sizeOfPackage + " Mb");
		System.out.println("------------------ Time spent: " + (elapsedTime / 1000000000) + " seconds");
		System.out.printf("------------------ Flow Rate: %.5f %s", elapsedTimeToSend, " Mb/s \n");
		System.out.println("------------------------------------------------------------");

		out.close();
		in.close();
		stdIn.close();
		echoSocket.close();
	}
}
